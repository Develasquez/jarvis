var express = require('express');
var router = express.Router();
var locations= require('../models/_locations');
_idToString = function (r) {
 for (i=0;i<r.length;i++){
 r[i]._id = r[i]._id.toString();
 }
return r
}
 _toList = function (req, res) {
      res.redirect('locations/list');
};
 _list = function(req, res){
 locations.list([],0,function(r,f) {
     try{
         r= _idToString(r);
         if (req.params.format !="json"){
             res.render('locations', { title: 'Locations', locations : r });
         }else{
             res.send({data:r})
         }
     }catch(ex){
     }
 })
};
 _new = function(req, res){
 res.render('locations_new', { title: 'new locations' });
};
 _delete = function(req, res){
 var _id = req.params.id;
 locations.delete([],_id,function(r,f) {
     try{
         r= _idToString(r);
         res.render('locations', { title: 'Locations', locations : r });
     }catch(ex){
     }
 })
};
 _update = function(req, res){
 var _id = req.params.id;
 if (_id){
 locations.update(req.body,_id,function(r,f) {
     try{
         r= _idToString(r);
         res.render('locations_new', { title: 'Locations', locations : r[0] });
     }catch(ex){
     }
 })
}else{
 locations.insert(req.body,function(r,f) {
     try{
         r = _idToString(r);
         res.render('locations_new', { title: 'Locations', locations : r[0] });
     }catch(ex){
     }
     })
 }
};
 _range = function(req, res){
	res.render('locations_range', { title: 'locations' });
};
 _get = function(req, res){
     var _id = req.params.id.split(".")[0];
     var _format = req.params.id.split(".")[1];
     locations.find(_id,function(r,f) {
     try{
        r = _idToString(r);
         if (_format !="json"){
            res.render('locations_new', { title: 'Locations', locations : r[0] });
         }else{
             res.send({data:r})
         }
     }catch(ex){
  }
})
};
/*
Excaffold generated routes
*/
router.get('/',_toList);
router.get('/list',_list);
router.get('/list.:format/',_list);
router.get('/new', _new);
router.post('/new', _update);
router.get('/:id/delete', _delete);
router.delete('/:id', _delete);
router.get('/:id/update', _update);
router.get('/:id/range',_range);
router.get('/:id', _get);
router.post('/:id', _update);
router.put('/:id', _update);
module.exports = router;