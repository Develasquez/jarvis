var search = require('youtube-search');
var request = require('request');
var opts = {
	maxResults: 10,
	key: 'AIzaSyAucyHu26je9-g2tW-7F-sQdfKX55xtl8c'
};

module.exports = {
	getMusic: function(name) {
		return new Promise(function(resolve, reject) {
			search(name, opts, function(err, results) {
				
				if (err) return console.log(err);
				results = results.filter(function(el){ return el.kind === "youtube#video"});

				var videoId = results[0].id;
				request.get("http://desamovil.cl:3001/mp3/?url=https://www.youtube.com/watch?v=" + videoId, function(err, body , data) {
					resolve(JSON.parse(data).url);
				});
			});
		});

	}
}