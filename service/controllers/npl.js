var wit = require("./wit");
var intents = require("./intents");
var DEFAULT_MESSAGE = {type: "response",
					content: {
						message: "No tengo operaciones para esa solicitud"
					},
					category: "text"};
var _ = require("lodash");

module.exports = {
	intent: function(request) {
		return new Promise(function(resolve, reject) {
			var text = request.text;
			var position = request.position;
			var context = request.context;
			wit.proccess(text).then(function(data) {
				data.position = position;
				var intent = wit.entityValue(data.entities, 'intent', 0);
				intents[intent](data).then(function(result) {
					try {
						result.analysis= JSON.stringify(data);
						result.intent= intent;
					} catch (ex) {
						result = DEFAULT_MESSAGE;
					}
					resolve(result);
				});
			}).catch(function(err) {
				console.log(err);
				reject({
					err: err
				});
			});
		});
	}
};