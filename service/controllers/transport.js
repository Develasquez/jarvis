var urlFelieados = "https://feriados-cl-api.herokuapp.com/feriados";


var request = require("request");
var transportController = {
	getStopsByGeolocation: function(geo) {
			var url = "http://www.transantiago.cl/restservice/rest/getpuntoparada?lat=" + geo.lat + "&lon=" + geo.lon + "&bip=1";
		return new Promise(function(resolve, reject) {
			request(url, function(error, response, body) {
				if (error) {
					reject(error);
					return;
				}
				resolve(response);
			});
		});
	},
	getTimeByStopAndService: function(stopCode, serviceCode) {
		var url = "http://www.transantiago.cl/predictor/prediccion?codsimt=" + stopCode + "&codser=" + serviceCode;
		return new Promise(function(resolve, reject) {
			request(url, function(error, response, body) {
				if (error) {
					reject(error);
					return;
				}
				resolve(response);
			});
		});
	},
	getTransportListByGeolocation: function() {},
	setTrasnport: function() {}
}