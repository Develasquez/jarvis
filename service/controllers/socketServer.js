var naturalLanguageProcessing = require("./npl");
module.exports = {
  startServer: function(server) {
    var io = require('socket.io').listen(server);
    console.log("init");
    io.on('connection', function(socket) {
      console.log("connection");
      socket.on('request', function(request) {
        console.log(request); 
        naturalLanguageProcessing.intent(request)
          .then(function(result) {
            socket.emit("response", result);
          }).catch(function(err) {
            socket.emit("error", err);
          });
      })
    });
  }
};