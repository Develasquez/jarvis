var moment = require("moment");
moment.locale('es');
var request = require("request");
var _ = require("lodash");
var wit = require("./wit");
var GOOGLE_API_KEY = "AIzaSyBpBcUktgEtSUh99nx0hF45XssQcEj3aJA";
var weather = require("./weather");
var commands = require("./commands");
var cheerio = require("cheerio");

var intents = {
	GPS: function(data) {
		return new Promise(function(resolve, reject) {
			var action = wit.entityValue(data.entities, 'action', 0) ||
				wit.entityValue(data.entities, 'controls', 0);
			if (data.entities.action) {


				if (data.entities.action.filter((a) => a.value == "list").length) {
					resolve({
						type: "response",
						"content": {
							message: "Todas las estaciones",
							scope: "all"
						},
						"category": "map"
					});
					return;
				}
				if (data.entities.action.filter((a) => a.value == "near").length) {
					resolve({
						type: "response",
						"content": {
							message: "Cercanas a ti",
							scope: "near",
						},
						"category": "map"
					});
					return;
				}
			}
			if (action == "search") {
				var message = data._text.toLowerCase().split(/a |al |hasta /g);
				resolve({
					type: "response",
					"content": {
						message: message[message.length - 1] || "a Walli",
					},
					"category": "directions"
				});
				return;
			}
			if (action == "show") {
				resolve({
					type: "response",
					"content": {
						message: data._text.toLowerCase().split(/ubicación|mapa|está|esta/g)[1] || "a Wali",
					},
					"category": "geocode"
				});
				return;
			}
			if (action == "minutos") {

				var message = data._text.toLowerCase().split(/a |al |hasta /g) ;
				resolve({
					type: "response",
					"content": {
						message: message[message.length - 1] || "a Wali",
					},
					"category": "distance"
				});
				return;
			}
		});
	},
	stop: function() {
		return new Promise(function(resolve, reject) {
			resolve({
				type: "response",
				content: {
					message: "stop"
				},
				category: "text"
			});
		});
	},
	commands: function() {
		return new Promise(function(resolve, reject) {
			resolve({
					type: "response",
					content: {
						message: commands.available()
					},
					category: "list"
				}


			);
		});
	},
	greet: function(data) {
		return new Promise(function(resolve, reject) {
			var nombre = wit.entityValue(data.entities, 'contact', 0);

			if (nombre) {
				resolve({
					type: "response",
					content: {
						message: "Hola " + nombre + " ¿como estas?, es un gusto saludarlo"
					},
					category: "text"
				});
			} else {
				resolve({
					type: "response",
					content: {
						message: "Hola gusto en saludarle"
					},
					category: "text"
				});
			}
		});
	},
	time: function(data) {
		return new Promise(function(resolve, reject) {
			var time = moment(new Date()).format("hh:mm:ss");
			var text = "Son las " + time;
			resolve({
				type: "response",
				content: {
					message: text
				},
				category: "text"
			});
		});
	},
	holydays: function(data) {
		return new Promise(function(resolve, reject) {
			var url = "https://feriados-cl-api.herokuapp.com/feriados";
			request(url, function(error, response, body) {
				var days = JSON.parse(body);
				days = days.filter(function(day) {
					return moment(day.date).diff(moment(new Date)) > 0;
				});
				days = days.map(function(day) {
					return "El día " + moment(day.date).format("DD") + " de " + moment(day.date).format("MMMM") + " es: " + day.title + "\n";
				})

				if (error) {
					reject(error);
					return;
				}
				resolve({
					type: "response",
					content: {
						message: [days[0], days[1], days[2]]
					},
					category: "list"
				});
			});
		});
	},
	transport: function(data) {
		return new Promise(function(resolve, reject) {
			resolve("Aun tiene que implementar esta función ");
		});
	},
	music: function(data) {
		return new Promise(function(resolve, reject) {
			var cancion = data._text.split(/cancion|canción|escuchar|musica|música|reproduce|tema/);

			var music = require("./music");
			music.getMusic(cancion[cancion.length - 1]).then(function(url) {
				resolve({
					type: "response",
					content: {
						message: url
					},
					category: "music"
				});
			});
		});
	},
	weather: weather.proccess,
	todo: function(data) {
		return new Promise(function(resolve, reject) {
			var synonyms = "avísame|avisame|comprar|llamar|pendiente|pendientes|recordatorio|recuerda|recuerdame";
			var wantList = wit.entityValue(data.entities, 'action', 0) == "list";
			var date = wit.entityValue(data.entities, 'datetime', 0) || new Date(moment(new Date()).format("MM/DD/YYYY"));
			var time = "";
			var todo = require("../models/_todo");
			var alarm = !!time;
			if (wantList) {
				todo.find({
					"done": 0
				}, function(docs, err) {
					var todoList = docs.map(function(el) {
						return {
							id: el._id.toString(),
							text: el.text
						};
					});
					resolve({
						type: "response",
						content: {
							message: todoList
						},
						category: "todos"
					});
				});
			} else {
				todo.insert({
					text: data._text.replace(new RegExp(synonyms), ''),
					date: new Date(date),
					done: 0,
					alarm: alarm
				}, function() {
					resolve({
						type: "response",
						content: {
							message: "Recordatorio añadido"
						},
						category: "text"
					});
				});
			}
		});
	},
	calc: function(data) {
		return new Promise(function(resolve, reject) {
			var unit = wit.entityValue(data.entities, 'unit', 0);
			var action = wit.entityValue(data.entities, 'action', 0);


			if (action == "minutos") {

				var message = data._text.toLowerCase().split(/estoy|con|demora|demoro|falta|minutos|tardo|tiempo|tengo|hasta/g);
				resolve({
					type: "response",
					"content": {
						message: message[message.length - 1] || "a Wali",
					},
					"category": "distance"
				});
				return;
			}

			function round(num) {
				return (Math.round(num * 100) / 100).toFixed(2)
			}
			var stringNumber = [
				["uno", 1],
				["un", 1],
				["dos", 2],
				["tres", 3],
				["cuatro", 4],
				["cinco", 5],
				["seis", 6],
				["siete", 7],
				["ocho", 8],
				["nueve", 9]
			];
			var numbers = _.compact(data._text.replace(/ coma | koma | como | punto /gi, ".").split(/[^0-9.,]/));

			num1 = parseFloat(numbers[0]);
			num2 = parseFloat(numbers[1]);


			if (unit) {
				var result = "";
				var momentoActual = new Date();
				var dia = momentoActual.getDate();
				var mes = momentoActual.getMonth() + 1;
				var ano = momentoActual.getFullYear();

				var unitsActions = {
					"TIME": function() {
						var message = data._text.toLowerCase().split(/estoy|con|demora|demoro|falta|minutos|tardo|tiempo|tengo|hasta/g);
						resolve({
							type: "response",
							"content": {
								message: message[message.length] || "a Wali",
							},
							"category": "distance"
						});

					},
					"UF": function() {
						var servicio = "uf";
						var urlUF = "http://api.sbif.cl/api-sbifv3/recursos_api/" + servicio + "/" + ano + "/" + mes + "/?apikey=fff9cd0d852f6bb3330fc7c55978761603cbcb10&formato=json";
						request(urlUF, function(error, response, body) {
							var data = JSON.parse(body)
							var ufHoy = data.UFs[data.UFs.length - 1].Valor.replace(".", "").replace(",", ".");
							if (num1) {
								result = num1 + " UFs son " + round(ufHoy * num1) + " pesos";
							} else {
								result = "La Unidad de Fomento el día de hoy esta a: " + ufHoy + " pesos";
							}
							resolve({
								type: "response",
								content: {
									message: result
								},
								category: "text"
							});
						});
					},
					"EUR": function() {
						var servicio = "euro";
						var urlUF = "http://api.sbif.cl/api-sbifv3/recursos_api/" + servicio + "/" + ano + "/" + mes + "/?apikey=fff9cd0d852f6bb3330fc7c55978761603cbcb10&formato=json";
						request(urlUF, function(error, response, body) {
							var data = JSON.parse(body);
							var euroHoy = data.Euros[data.Euros.length - 1].Valor.replace(".", "").replace(",", ".");
							if (num1) {
								result = num1 + " Euros son " + round(euroHoy * num1) + " pesos";
							} else {
								result = "El Euro el día de hoy esta a: " + euroHoy + " pesos";
							}
							resolve({
								type: "response",
								content: {
									message: result
								},
								category: "text"
							});
						});
					},
					"ARS": function() {},
					"IVA": function() {},
					"UTM": function() {},
					"USD": function() {
						var servicio = "dolar";
						var urlUF = "http://api.sbif.cl/api-sbifv3/recursos_api/" + servicio + "/" + ano + "/" + mes + "/?apikey=fff9cd0d852f6bb3330fc7c55978761603cbcb10&formato=json";
						request(urlUF, function(error, response, body) {
							var data = JSON.parse(body);
							var dolarHoy = data.Dolares[data.Dolares.length - 1].Valor.replace(".", "").replace(",", ".");
							if (num1) {
								result = num1 + " Dólares son " + round(dolarHoy * num1) + " pesos";
							} else {
								result = "El Dólar el día de hoy esta a: " + dolarHoy + " pesos";
							}
							resolve({
								type: "response",
								content: {
									message: result
								},
								category: "text"
							});
						});
					}
				};

				unitsActions[unit]();
			} else {



				_.forEach(stringNumber, function(strNum) {
					data._text = data._text.replace(new RegExp(strNum[0], "gi"), strNum[1]);
				})
				var operator = wit.entityValue(data.entities, 'operator', 0);


				var functions = {
					addition: function() {
						return "El resultado es :" + round(num1 + num2);
					},
					subtraction: function() {
						return "El resultado es :" + round(num1 - num2);
					},
					multiplication: function() {
						return "El resultado es :" + round(num1 * num2);
					},
					division: function() {
						return "El resultado es :" + round(num1 / num2);
					},
					percent: function() {
						return "El resultado es :" + round(num1 * num2 / 100);
					}
				};
				var result = functions[operator]().replace(".00", "");
				resolve({
					type: "response",
					content: {
						message: result
					},
					category: "text"
				});
			}
		});
	},
	calendar: function(data) {
		return new Promise(function(resolve, reject) {
			resolve("Aun tiene que implementar esta función ");
		});
	},
	findPhone: function(data) {
		return new Promise(function(resolve, reject) {
			resolve("Aun tiene que implementar esta función ");
		});
	},
	recipes: function(data) {
		return new Promise(function(resolve, reject) {
			resolve("Aun tiene que implementar esta función ");
		});
	},
	cameras: function(data) {
		return new Promise(function(resolve, reject) {
			resolve("Aun tiene que implementar esta función ");
		});
	},
	email: function(data) {
		return new Promise(function(resolve, reject) {
			resolve("Aun tiene que implementar esta función ");
		});
	},
	jw: function(data) {
		return new Promise(function(resolve, reject) {
			var momentoActual = new Date();
			var dia = momentoActual.getDate();
			var mes = momentoActual.getMonth() + 1;
			var ano = momentoActual.getFullYear();
			var accion = "dailyText";
			var urlJw = "https://wol.jw.org/es/wol/dt/r4/lp-s/" + ano + "/" + mes + "/" + dia;
			request(urlJw, function(error, response, body) {
				var $ = cheerio.load(body.toString());
				var dailyText = $(".itemData")
					.eq(0)
					.text()
					.split(/\n|,/g)
					.filter(function(el) {
						return el.trim().length > 0;
					});
				resolve({
					type: "response",
					content: {
						message: dailyText
					},
					category: "list"
				});
			});
		});
	},
	economicIndicators: function(data) {
		return new Promise(function(resolve, reject) {
			resolve("Aun tiene que implementar esta función ");
		});
	},
	wikipedia: function(data) {
		return new Promise(function(resolve, reject) {
			resolve("Aun tiene que implementar esta función ");
		});
	},
	google: function(data) {
		return new Promise(function(resolve, reject) {
			var hotWord = /Busca en Google|Buscar en Google/gi;
			var url = "https://www.googleapis.com/customsearch/v1?key=" + GOOGLE_API_KEY + "&cx=001323175733443053778:qgql1nucp2k&q=" + data._text.replace(hotWord, "");
			request(url, function(error, response, body) {
				var result = JSON.parse(body).items[0].snippet;
				resolve({
					type: "response",
					content: {
						message: dailyText
					},
					category: "text"
				});
			});
		});
	},
	alarm: function(data) {
		return new Promise(function(resolve, reject) {
			resolve("Aun tiene que implementar esta función ");
		});
	},
	news: function(data) {
		return new Promise(function(resolve, reject) {
			resolve("Aun tiene que implementar esta función ");
		});
	},

};

module.exports = intents;