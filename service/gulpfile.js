var gulp = require('gulp');
var browserSync = require('browser-sync').create();

gulp.task('browserSync', function() {
    browserSync.init({
        server: {
            baseDir: ''
        },
    })
})

gulp.task('default', ['server']);

// Minify CSS
gulp.task('server', function() {
    return browserSync.reload({
            stream: true
        });
});